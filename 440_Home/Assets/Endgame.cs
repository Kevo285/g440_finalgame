﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Endgame : MonoBehaviour
{
    public GameObject endofgame;
    public Text dialogue;
    public string speech;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter(Collider myTrigger)
    {
        if (myTrigger.gameObject.CompareTag("Player"))
        {


            dialogue.text = speech;


        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            dialogue.text = " ";
            endofgame.SetActive(true);
            Application.Quit();
        }

    }
}
